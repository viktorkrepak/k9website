"use script";

// Multi-Step Form
let currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("form-step");
  if (x.length > 0) {
    x[n].style.display = "flex";
    //... and fix the Previous/Next buttons:
    if (n == 0) {
      let pButton = document.getElementById("prevBtn");
      if (pButton) {
        pButton.style.opacity = "0";
        pButton.style.pointerEvents = "none";
      }
      let u = document.querySelectorAll(".have-account-block");
      u.forEach(function(item) {
        item.style.opacity = "0";
        item.style.pointerEvents = "none";
        return item;
      });
    } else {
      let pButton = document.getElementById("prevBtn");
      if (pButton) {
        pButton.style.opacity = "1";
        pButton.style.pointerEvents = "all";
      }
      let u = document.querySelectorAll(".have-account-block");
      u.forEach(function(item) {
        item.style.opacity = "1";
        item.style.pointerEvents = "all";
        return item;
      });
    }
    if (n == x.length - 1) {
      let pButton = document.getElementById("nextBtn");
      if (pButton) {
        pButton.style.opacity = "0";
        pButton.style.pointerEvents = "none";
      }
    } else {
      let pButton = document.getElementById("nextBtn");
      if (pButton) {
        pButton.style.opacity = "1";
        pButton.style.pointerEvents = "all";
      }
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n);
    fixTotalStepIndicator();
  }
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("form-step");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("useGiftForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fта яields
  var x,
    y,
    i,
    valid = true;
  x = document.getElementsByClassName("form-step");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:

  // for (i = 0; i < y.length; i++) {
  //     // If a field is empty...
  //     if (y[i].value == "") {
  //         // add an "invalid" class to the field:
  //         y[i].className += " invalid";
  //         // and set the current valid status to false
  //         valid = false;
  //     }
  // }
  // // If the valid status is true, mark the step as finished and valid:
  // if (valid) {
  //     document.getElementsByClassName("step")[currentTab].className += " finish";
  // }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  let i,
    x = document.getElementsByClassName("step");
  if (x.length > 0) {
    for (i = 0; i < x.length; i++) {
      x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
    let stepId = document.getElementById("useGiftCurrentStep");
    stepId.innerText = n + 1;
  }
}

function fixTotalStepIndicator() {
  let x = document.getElementsByClassName("step");
  if (x.length > 0) {
    let total = document.getElementById("useGiftTotalStep");
    if (total) {
      total.innerText = parseInt(x.length);
    }
  }
}
jQuery(function($) {
  $('input:checkbox[name="same-address-check"]').change(function() {
    if ($(this).is(":checked")) {
      $("#billingAdressField").addClass("remove-block");
      $("#saveData-1").addClass("remove-block");
    } else {
      $("#billingAdressField").removeClass("remove-block");
      $("#saveData-1").removeClass("remove-block");
    }
  });

  $(".navbar-toggler").on("click", function() {
    $(this).toggleClass("open");
    $(".navbar-collapse").toggleClass("active");
    $("body").toggleClass("menu-open");
  });

  let scrollNavLink = $(".rules-nav-link");

  scrollNavLink.on("click", function(event) {
    event.preventDefault();
    var id = $(this).attr("href"),
      top = $(id).offset().top - 25;
    $("body,html").animate(
      {
        scrollTop: top
      },
      750
    );
    $(".rules-nav-link").removeClass("active");
    $(this).toggleClass("active");
  });

  // $(scrollNavLink).on('scroll', function () {
  //     var id = $(this).attr('href'),
  //         scrollNavBlock = $(id).offset().top;
  //     console.log(id);
  //     let scrollTop = $(this).scrollTop();
  //     if (scrollTop >= scrollNavBlock) {
  //         $(scrollNavLink).addClass('active');
  //     } //else {
  //     //     $('#openbtn').removeClass('openbtnScroll');
  //     // }
  // });
  var delay = 1000;
  $(window).scroll(function() {
    console.log($(this).scrollTop());
    var top_show = 1000;
    var absolute_show = 5400;
    if ($(this).scrollTop() > top_show && $(this).scrollTop() < absolute_show) {
      $("#toTop").fadeIn();
      $("#toTop").removeClass("absolute-position");
      console.log(`I'm here ${top_show}`);
    } else if ($(this).scrollTop() > absolute_show) {
      $("#toTop").addClass("absolute-position");
      console.log(`I'm here ${absolute_show}`);
    } else {
      $("#toTop").fadeOut();
      $("#toTop").removeClass("absolute-position");
    }
  });
  $("#toTop").click(function() {
    $("body, html").animate(
      {
        scrollTop: 0
      },
      delay
    );
  });

  $(".testimonials-slider").slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    fade: true,
    speed: 500,
    cssEase: "linear",
    autoplay: true,
    autoplaySpeed: 4000
  });
  $(".latest-posts-slider").slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 4000
  });

  $(".regular-slider").slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    variableWidth: true,
    centerPadding: 0,
    arrows: true,
    dots: true,
    autoplay: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          variableWidth: false,
          slidesToShow: 1,
          centerPadding: "40px"
        }
      }
    ]
  });

  $(".view-password-btn").on("click", function(e) {
    e.preventDefault();
  });
  $(".view-password-btn").on("mousedown", function(e) {
    let input = $(this).siblings("input");
    if (typeof input !== "undefined") {
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      }
    }
  });
  $(".view-password-btn").on("mouseup", function(e) {
    let input = $(this).siblings("input");
    if (typeof input !== "undefined") {
      if (input.attr("type") == "text") {
        input.attr("type", "password");
      }
    }
  });

  //posts filter
  $(".blog-filter-label").on("click", function(e) {
    var item = $(this);
    filterPosts(item);
  });

  function filterPosts(item = false) {
    setTimeout(function() {
      if (item) {
        let input = item.siblings("input");
        var link = "";
        if (input.attr("name") != "all") {
          $("input[name='all']").prop("checked", false);
          let checkboxes = $(".blog-filter-checkbox:checked");
          if (checkboxes.length > 0) {
            link = "?categories=";
            $.each(checkboxes, function(i, e) {
              if (
                checkboxes.eq(i).prop("checked") == true &&
                checkboxes.eq(i).attr("name") != "all"
              ) {
                link = link + "+" + checkboxes.eq(i).attr("name");
              }
            });
          }
        } else if (input.prop("checked") && input.attr("name") == "all") {
          $("input[name!='all']").prop("checked", false);
        }
        link =
          location.protocol + "//" + location.host + location.pathname + link;
        document.location.href = link;
      }
    }, 100);
  }
  //posts load more
  $("#load-more-posts-btn").on("click", function() {
    var url = new URL(location.href);
    var searchParams = new URLSearchParams(url.search.substring(1));
    var categories = searchParams.get("categories");
    var numberofposts = parseInt($(this).data("counter"));
    numberofposts = numberofposts + 6;
    var maxnumberposts = parseInt($(this).data("max"));
    $.ajax({
      url: k9_ajax_url,
      type: "post",
      data: {
        action: "k9_load_more_posts",
        categories: categories,
        counter: numberofposts
      },
      success: function(response) {
        $("#tails-row").html(response);
        if (numberofposts < maxnumberposts) {
          $("#load-more-posts-btn").data("counter", numberofposts + 6);
        } else {
          $("#load-more-posts-btn").hide();
        }
      }
    });
  });

  let stateTitle = $(".state-title");

  let stateInput = $(".state-input");

  stateInput.on("click", function() {
    $(this).addClass("input-focus");
    $(".state-autocomplete-result").addClass("active");
  });

  $(document).mouseup(function(e) {
    var block = $(".state-autocomplete-result");
    var btn = stateInput;
    if (
      !block.is(e.target) &&
      !btn.is(e.target) &&
      block.has(e.target).length === 0
    ) {
      block.removeClass("active");
      btn.removeClass("input-focus");
    }
  });

  $(".key-btn").on("click", function() {
    $(this).toggleClass("btn-clue-active");
    $(this)
      .next(".clue-block")
      .toggleClass("active-block");
  });

  $(".map-part").on("click", function() {
    $("body").addClass("body-modal-open");
    $("#mapSelectModal").addClass("active");
    console.log($(this).data("country"));
    if ($(this).data("country")) {
      $(".mapselect-modal .select-country-span").text($(this).data("country"));
    } else {
      $(".mapselect-modal .select-country-span").text("Eldorado");
    }
  });

  $(".mapselect-modal .close-modal-btn").on("click", function() {
    $("body").removeClass("body-modal-open");
    $("#mapSelectModal").removeClass("active");
  });
  $(".mapselect-modal .btn-alt-2").on("click", function() {
    $("body").removeClass("body-modal-open");
    $("#mapSelectModal").removeClass("active");
  });
  $(".subscribe-mob-slider").slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
    centerMode: true,
    slidesToShow: 1,
    centerPadding: "90px",
    arrows: false,
    dots: false,
    autoplay: false,
    autoplaySpeed: 4000,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 769,
        settings: "unslick"
      }
    ]
  });

  stateTitle.on("click", function() {
    setTimeout(() => {
      stateInput.text($(this).text());
      $("#hiddenStateInput").val($(this).data("value"));
      $(".state-autocomplete-result").removeClass("active");
    }, 150);

    console.log($(this).data("value"));
  });
});

$("#tab_selector").on("change", function(e) {
  $(".nav-tabs a")
    .eq($(this).val())
    .tab("show");
});
